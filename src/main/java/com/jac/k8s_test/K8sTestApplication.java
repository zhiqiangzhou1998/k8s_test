package com.jac.k8s_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8sTestApplication.class, args);
	}

}

package com.jac.k8s_test.entity;


import lombok.Builder;
import lombok.Data;
@Builder(toBuilder = true)
@Data
public class Car {
    private String name;
    private int year;
}

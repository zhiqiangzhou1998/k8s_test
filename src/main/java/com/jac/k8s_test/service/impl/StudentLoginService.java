package com.jac.k8s_test.service.impl;


import com.jac.k8s_test.controller.LoginController;
import com.jac.k8s_test.entity.User;
import com.jac.k8s_test.repo.UserRepository;
import com.jac.k8s_test.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentLoginService implements LoginService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public Boolean register(LoginController.RegisterRequest registerRequest) {
        User user = userRepository.findByUserName(registerRequest.getUserName());
        if (user != null) {
            return false;
        } else {
            userRepository.save(new User().setUserName(registerRequest.getUserName()).setPassword(registerRequest.getPassword()));
            return true;
        }
    }
}

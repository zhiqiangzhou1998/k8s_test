package com.jac.k8s_test.controller;


import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TestController {

    @GetMapping("/whoWeAre")
    @ApiOperation(value = "666")
    public Map<String, String> getData() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("WEB SERVICES","JAC-FSD09-TEAM2");
        return hashMap;
    }



    @GetMapping("/testMR")
    @ApiOperation(value = "8888")
    public Map<String, String> testMR() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("aaaa", "bbbb");
        return hashMap;
    }
}

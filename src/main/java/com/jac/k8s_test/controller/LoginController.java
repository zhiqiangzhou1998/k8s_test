package com.jac.k8s_test.controller;


import com.jac.k8s_test.service.LoginService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Data
    public static class RegisterRequest {
        @NotBlank(message = "username can no be null")
        private String userName;
        @NotBlank(message = "password can no be null")
        private String password;
    }


    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest registerRequest) {
        Boolean register = loginService.register(registerRequest);
        if (register) {
            return ResponseEntity.ok("register successful");
        } else {
            return ResponseEntity.ok("username already exist");
        }

    }
}
